CicleProgressBar
================

A progress bar in the form of a circle written using Qt5. It is easy to configure different start/stop angles and colors.

![Example](https://i.imgur.com/5ewWfLh.png)

This project is not an example of good programming practice. It is more me exploring Qt/C++ programming.