/*    CircleProgressBar: A circular progress bar written with Qt5.
 *    Copyright (C) 2020  Tijl Schepens
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "circledprogressbar.h"
#include <QPainter>

CircledProgressBar::CircledProgressBar(QWidget *parent) : QWidget(parent),
    min(0), max(100), progress(100), colors{Qt::green, Qt::black}
{
    QSizePolicy pol = QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    setSizePolicy(pol);
}


QSize CircledProgressBar::sizeHint() const
{
    return QSize(200, 200);
}

int CircledProgressBar::getProgress()
{
    return progress;
}

void CircledProgressBar::setProgress(int prog)
{
    if(prog < min || prog > max){
        qFatal("Progress is out of bounds. Min: %d, Max: %d, Progress: %d",
               min, max, prog);
    } else{
        progress = prog;
    }
}

void CircledProgressBar::setLimits(int nMin, int nMax)
{
    if(nMin >= nMax){
        qFatal("Minimum is larger or equal to maximum: %d >= %d",
               nMin, nMax);
    } else{
        min = nMin;
        max = nMax;
    }

    if(progress > max || progress < min){
        setProgress(max);
    }
}

void CircledProgressBar::setLimits(QVector<int> limits)
{
    if(limits.length() != 2){
        qFatal("Wrong QVector length. Expected 2, got: %d",
               limits.length());
    } else if(limits[0] >= limits[1]){
        qFatal("Minimum is larger or equal to maximum: %d >= %d",
               limits[0], limits[1]);
    } else{
        min = limits[0];
        max = limits[1];
    }

    if(progress > max || progress < min){
        setProgress(max);
    }
}

QVector<int> CircledProgressBar::getLimits()
{
    QVector<int> limits(2);
    limits[0] = min;
    limits[1] = max;

    return limits;
}

void CircledProgressBar::setColors(QColor startColor, QColor endColor)
{
    colors[0] = startColor;
    colors[1] = endColor;
}

void CircledProgressBar::paintEvent(QPaintEvent *)
{
    QPainter mPainter(this);
    double whSize;

    whSize = qMin(width(), height());

    /* We convert the progress value into an angle which is used to draw our outer circle.
     * The outer circle starts at -40deg and stops at 220deg. This means that 260deg
     * is 100% progress.
     */
    double progAngle = (static_cast<double>(progress - min)/(max - min)) * 260;

    QRectF size(QPointF(10.0, 10.0), QPointF(whSize-10.10, whSize-10.0));
    mPainter.setPen(Qt::transparent);
    /* Draw a base white circle. */
    mPainter.setBrush(Qt::white);
    mPainter.drawPie(size, -140*16, -260*16);

    /* Construct a gradient brush to draw the outer circle. */
    QConicalGradient conGradient(whSize/2, whSize/2, 240);
    conGradient.setColorAt(0, colors[1]);
    conGradient.setColorAt(1, colors[0]);
    QBrush pBrush(conGradient);
    /* Draw a colored gradient circle. */
    mPainter.setBrush(pBrush);
    mPainter.drawPie(size, -140*16, -progAngle*16);

    /* Draw a circle above it which inherits the background color to make it look transparent. */
    size.setTopLeft(QPointF(50.0, 50.0));
    size.setBottomRight(QPointF(whSize-50.10, whSize-50.0));
    mPainter.setBrush(QWidget::palette().color(QWidget::backgroundRole()));
    mPainter.drawPie(size, 0, 360*16);
}
