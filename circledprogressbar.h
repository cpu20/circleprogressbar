/*    CircleProgressBar: A circular progress bar written with Qt5.
 *    Copyright (C) 2020  Tijl Schepens
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CIRCLEDPROGRESSBAR_H
#define CIRCLEDPROGRESSBAR_H

#include <QWidget>

class CircledProgressBar : public QWidget
{
    Q_OBJECT
public:
    explicit CircledProgressBar(QWidget *parent = nullptr);
    /* Getters and setters. */
    int getProgress();
    void setProgress(int progress);
    void setLimits(int nMin, int nMax);
    void setLimits(QVector<int> limits);
    QVector<int> getLimits();
    void setColors(QColor startColor, QColor endColor);
    virtual QSize sizeHint() const;

protected:
    void paintEvent(QPaintEvent *);

private:
    int min, max, progress;
    QColor colors[2];

signals:

};

#endif // CIRCLEDPROGRESSBAR_H
